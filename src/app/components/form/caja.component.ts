import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-caja',
  templateUrl: './caja.component.html',
  styleUrls: ['./caja.component.css']
})
export class FormComponent implements OnInit {

  formulario_caja!: FormGroup;
  caja_texto: string[] = []


  constructor(private fb: FormBuilder) {
    this.crear_Formulario();

  }

  crear_Formulario(): void {
    this.formulario_caja = this.fb.group({
      cajaGuargar: [''],
      caja: this.fb.array([[]]),

    })

  }

  ngOnInit(): void {
  }

  get caja_contenido() {
    return this.formulario_caja.get('caja') as FormArray
  }
  agregar_caja(): void {
    this.caja_contenido.push(this.fb.control('', Validators.required))
  }
  borrar(i: number): void {
    this.caja_contenido.removeAt(i);
  }
  limpiar(): void {
    this.caja_texto = ['']
    this.formulario_caja.reset
  }
  guardar(): void {
    console.log('guardar');
    this.caja_texto = this.formulario_caja.value.caja
  }


  limpiarCaja(): void {
    this.caja_texto = ['']
  }

}
